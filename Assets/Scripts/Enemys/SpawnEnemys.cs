using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.PlayerSettings;

public class SpawnEnemys : MonoBehaviour
{
    public GameObject Enemy;
    public GameObject Enemy2;
    private float _timer = 0;
    private float _cooldown = 1f;
    private float _x;
    private float _y;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _x = Camera.main.transform.position.x;
        _y = Camera.main.transform.position.y;
        _timer += Time.deltaTime;
        if (_timer > _cooldown)
        {
            Instantiate(Enemy, new Vector3(_x, _y+7f, 0), Quaternion.identity);
            _timer = 0;
        }

        int num = Random.Range(0, 15000);
        if (num == 4)
        {
            Instantiate(Enemy2, new Vector3(_x, _y + 7f, 0), Quaternion.identity);
        }
    }
}
