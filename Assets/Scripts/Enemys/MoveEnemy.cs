using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemy : MonoBehaviour
{
    private float _speed = 0.02f;
    private float _y;
    // Start is called before the first frame update
    void Start()
    {
        _y = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        _y = transform.position.y;

        if(_y > 10f)
        {
            transform.position += new Vector3(transform.position.x, _speed, 0);
        }
        else
        {
            transform.position -= new Vector3(transform.position.x, _speed, 0);
        }
        
    }
}
