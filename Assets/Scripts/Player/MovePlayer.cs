using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    [SerializeField] private float _speed = 0.02f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
        Move();
    }

    public void Move()
    {
        transform.position += new Vector3(_speed * Input.GetAxis("Horizontal"), _speed * Input.GetAxis("Vertical"), 0);
    }
}
