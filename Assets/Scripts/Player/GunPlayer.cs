using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPlayer : MonoBehaviour
{
    public GameObject Gun;
    private float _timer = 0;
    private float _cooldown = 0.01f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float posy = this.transform.position.y + 1;
        float posx = this.transform.position.x;
        _timer += Time.deltaTime;
        if (_timer > _cooldown)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                Instantiate(Gun, new Vector3(posx, posy, 0), Quaternion.identity);
            }
            _timer = 0;
        }
            
            
    }
}
